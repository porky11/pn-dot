use pns::{Net, SafeNet};

use std::{
    env::args,
    fs::File,
    io::{BufRead, BufReader},
};

fn indent(n: usize) -> String {
    "    ".repeat(n + 1)
}

fn start_level(n: usize, name: &str) {
    let indent = indent(n);
    println!("{}subgraph \"cluster {}\" {{", indent, name);
    println!("{}    label = \"{}\"", indent, name);
}

fn clear_path(path: &mut Vec<String>) {
    while let Some(_) = path.pop() {
        println!("{}}}", indent(path.len()));
    }
}

fn main() {
    let mut args = args();
    args.next();

    let net_file = args.next().expect("No net file specified");
    let net = Net::load(net_file.as_ref()).expect("Failed to load net");

    let mut reader = if let Some(names_path) = args.next() {
        let names_file = File::open(names_path).expect("Failed to open names file");
        Some(BufReader::new(names_file).lines())
    } else {
        None
    };

    println!("digraph {{");

    let mut current_path = Vec::new();
    let mut place_levels = vec![None; net.place_count as usize];

    println!("    // transitions");
    for (tid, transition) in net.transitions().into_iter().enumerate() {
        let label = if let Some(reader) = &mut reader {
            if let Some(Ok(line)) = reader.next() {
                if line.is_empty() {
                    continue;
                }

                let mut path: Vec<String> =
                    line.split(':').map(|segment| segment.to_string()).collect();
                let name = path.pop().unwrap();

                while current_path.len() > path.len() {
                    current_path.pop();
                    println!("{}}}", indent(current_path.len()));
                }

                let segments = path.into_iter();
                for (i, segment) in segments.enumerate() {
                    if i < current_path.len() {
                        if current_path[i] != segment {
                            while current_path.len() > i {
                                current_path.pop();
                                println!("{}}}", indent(current_path.len()));
                            }
                            start_level(i, &segment[..]);
                            current_path.push(segment);
                        }
                    } else {
                        start_level(current_path.len(), &segment[..]);
                        current_path.push(segment);
                    }
                }

                format!(", label = \"{}\"", name)
            } else {
                clear_path(&mut current_path);
                String::new()
            }
        } else {
            clear_path(&mut current_path);
            String::new()
        };

        println!(
            "{}t{} [shape = rect{}]",
            indent(current_path.len()),
            tid,
            label
        );

        for &pid in transition.prev() {
            if let Some(existing_path) = &place_levels[pid as usize] {
                let existing_path: &Vec<String> = existing_path;
                let mut new_path = Vec::new();
                for (new_element, existing_element) in current_path.iter().zip(existing_path) {
                    if new_element == existing_element {
                        new_path.push(existing_element.clone());
                    } else {
                        break;
                    }
                }
                place_levels[pid as usize] = Some(new_path);
            } else {
                place_levels[pid as usize] = Some(current_path.clone());
            }
        }

        for &pid in transition.next() {
            if let Some(existing_path) = &place_levels[pid as usize] {
                let existing_path: &Vec<String> = existing_path;
                let mut new_path = Vec::new();
                for (new_element, existing_element) in current_path.iter().zip(existing_path) {
                    if new_element == existing_element {
                        new_path.push(existing_element.clone());
                    } else {
                        break;
                    }
                }
                place_levels[pid as usize] = Some(new_path);
            } else {
                place_levels[pid as usize] = Some(current_path.clone());
            }
        }
    }

    clear_path(&mut current_path);

    println!();

    println!("    // places");
    for pid in 0..net.place_count {
        let count = net.initial_token_counts()[pid as usize];

        if let Some(path) = &place_levels[pid as usize] {
            while current_path.len() > path.len() {
                current_path.pop();
                println!("{}}}", indent(current_path.len()));
            }

            let segments = path.clone().into_iter();
            for (i, segment) in segments.enumerate() {
                if i < current_path.len() {
                    if current_path[i] != segment {
                        while current_path.len() > i {
                            current_path.pop();
                            println!("{}}}", indent(current_path.len()));
                        }
                        start_level(i, &segment[..]);
                        current_path.push(segment);
                    }
                } else {
                    start_level(current_path.len(), &segment[..]);
                    current_path.push(segment);
                }
            }
        } else {
            continue;
        }

        println!(
            "{}p{} [shape = circle, label = {}, color = {}, style = filled]",
            indent(current_path.len()),
            pid,
            count,
            if count == 0 { "red" } else { "blue" }
        );
    }

    clear_path(&mut current_path);

    println!();

    println!("    // relations");
    for (pid, place) in net.places().into_iter().enumerate() {
        for tid in place.prev() {
            println!("    t{} -> p{}", tid, pid)
        }

        for tid in place.next() {
            println!("    p{} -> t{}", pid, tid)
        }
    }

    println!("}}");
}
