# Usage

```
pn-dot FILE.pns [FILE.pnk]
```

Converts a petri net simulator file (`File.pns`) to a dot file.

You can generate petri nets using the [pns](https://gitlab.com/porky11/pns) library.
For example you can use [pn-editor](https://gitlab.com/porky11/pn-editor), a GUI based on it.

# Example

For example you could use some example files (`example.pns`, `example.pnk`) from the pns library, and convert them to a PDF using this command:

```
pn-dot example.pns example.pnk | dot -Tpdf example.pdf
```

